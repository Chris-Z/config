"        _
" __   _(_)_ __ ___  _ __ ___
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__
"   \_/ |_|_| |_| |_|_|  \___|
"

runtime! archlinux.vim
unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim
set t_Co=256

set nocompatible
set encoding=utf-8
syntax enable
" set visualbell
set wrap
set hidden
set ttyfast
" set laststatus=2 "always show status bar
set nu rnu
set numberwidth=3
runtime! plugin/**/*.vim
packadd! dracula
packadd! nord
packadd! gruvbox
set background=dark
"color nord
"color delek
"color dracula
color gruvbox
let g:gruvbox_contrast_dark='hard'
hi Normal guibg=NONE ctermbg=NONE





"let g:colorizer_auto_color = 1
let g:colorizer_auto_filetype='css,html,cpp'
"let g:colorizer_skip_comments = 1


set showcmd
set ignorecase smartcase

nnoremap n nzz
nnoremap N Nzz
" set mouse=a

set wildmode=longest,list,full
set splitbelow splitright

set confirm
set hlsearch

filetype on
filetype indent on

set path+=**

set timeout timeoutlen=100 "ttimeoutlen=100


let mapleader =";"


nnoremap <leader>; ;
inoremap <leader>; ;

nnoremap <leader>s :setlocal spell! spelllang=en_us<CR>
inoremap <leader>s <ESC>:setlocal spell! spelllang=en_us<CR>a

nnoremap <leader>w :w<ESC>
inoremap <leader>w <ESC>:w<CR>a

nnoremap <leader>x :!!<CR>
inoremap <leader>x <ESC>:!!<CR>

inoremap <leader>d <ESC>
nnoremap <leader>d <ESC>

inoremap <leader>f <TAB>
nnoremap <leader>f <TAB>

inoremap <leader>s <BS>
nnoremap <leader>s <BS>

" inoremap <leader>g
nnoremap <leader>g I" <ESC>
" inoremap <leader>G
nnoremap <leader>gg Ixx<ESC>

inoremap <leader>n <C-n>





inoremap <leader>r <ESC><c-r>
nnoremap <leader>r <c-r>

inoremap <leader>t <ESC>:vert term<CR>
nnoremap <leader>t :vert term<CR>

inoremap <leader>v <ESC>:vert<CR>
nnoremap <leader>v :vert<CR>

inoremap <leader>q <ESC>:q<CR>
nnoremap <leader>q :q<CR>

inoremap <leader>h <ESC><C-w>h
nnoremap <leader>h <C-w>h
inoremap <leader>j <ESC><C-w>j
nnoremap <leader>j <C-w>j
inoremap <leader>k <ESC><C-w>k
nnoremap <leader>k <C-w>k
inoremap <leader>l <ESC><C-w>l
nnoremap <leader>l <C-w>l


"inoremap <SPACE>a A
"inoremap <SPACE>b B
"inoremap <SPACE>c C
"inoremap <SPACE>d D
"inoremap <SPACE>e E
"inoremap <SPACE>f F
"inoremap <SPACE>g G
"inoremap <SPACE>h H
"inoremap <SPACE>i I
"inoremap <SPACE>j J
"inoremap <SPACE>k K
"inoremap <SPACE>l L
"inoremap <SPACE>m M
"inoremap <SPACE>n N
"inoremap <SPACE>o O
"inoremap <SPACE>p P
"inoremap <SPACE>q Q
"inoremap <SPACE>r R
"inoremap <SPACE>s S
"inoremap <SPACE>t T
"inoremap <SPACE>u U
"inoremap <SPACE>v V
"inoremap <SPACE>w W
"inoremap <SPACE>x X
"inoremap <SPACE>y Y
"inoremap <SPACE>z Z

"nnoremap <SPACE>a A
"nnoremap <SPACE>b B
"nnoremap <SPACE>c C
"nnoremap <SPACE>d D
"nnoremap <SPACE>e E
"nnoremap <SPACE>f F
"nnoremap <SPACE>g G
"nnoremap <SPACE>h H
"nnoremap <SPACE>i I
"nnoremap <SPACE>j J
"nnoremap <SPACE>k K
"nnoremap <SPACE>l L
"nnoremap <SPACE>m M
"nnoremap <SPACE>n N
"nnoremap <SPACE>o O
"nnoremap <SPACE>p P
"nnoremap <SPACE>q Q
"nnoremap <SPACE>r R
"nnoremap <SPACE>s S
"nnoremap <SPACE>t T
"nnoremap <SPACE>u U
"nnoremap <SPACE>v V
"nnoremap <SPACE>w W
"nnoremap <SPACE>x X
""nnoremap <SPACE>y Y
"nnoremap <SPACE>z Z



 

" set cursorline
" highlight CursorLine ctermbg=Gray cterm=bold guibg=#2b2b2b
" set cursorcolumn

" cnoremap jk <ESC><ESC>

" nnoremap ; :
" nnoremap ; ;


" set fillchars+=vert:-
" set clipboard+=unnamedplus "xclip or clipboard manager
"
"

set viewoptions-=options
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview
autocmd BufWinLeave * mkview
autocmd BufWinEnter * silent loadview

