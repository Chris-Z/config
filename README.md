# Config

A set of configuration files and scripts for my Linux system. Automates process of running applications, connecting to servers, and updating system applications. Goal is to optimize the operating system for programing efficiency by automating tasks.